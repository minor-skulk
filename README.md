# minor skulk, an alternative firefox accounts server

this tool only exists because [fxa](https://github.com/mozilla/fxa/) is nearly
impossible to self-host with adequate amounts of effort. while it is possible to
do and a couple of instructional repositories exist (mostly using a lot of
docker containers), even small installs of fxa use a lot of system resources.
additionally fxa is simply not made with self-hosting in mind and requires both
google *and* amazon cloud services to function properly outside of development
or test installations (which use emulators for those cloud services instead).

as such the scope of minor skulk is extremely limited: it exists to make sync
work. all other features of fxa are out of scope, including (but not limited
to):
 - payment processing
 - support for third-party applications
 - advanced user management
 - support tools
 - and others.

**WARNING**: no security audit of this project has been done. it may or may not
eat or expose your data. use at your own risk.

# requirements

 - postgresql 14 or newer
 - a local smtp server
 - a tls reverse proxy (for productive deployments)
 - a [syncstorage-rs](https://github.com/mozilla-services/syncstorage-rs) instance

# installation

minor skulk is built using cargo. rust version 1.61 and newer should work, older
versions have not been tested.

 * compile with `cargo build`
 * edit `Rocket.toml`, setting values for all the mandatory parameters
   * for test deployments it's sufficient to use the built-in http server. live
     deployments **must** use a reverse proxy to add TLS!
 * install [syncstorage-rs](https://github.com/mozilla-services/syncstorage-rs)
 * configure syncstorage-rs for a single-node setup as exemplified in
   [this nixos module](https://github.com/NixOS/nixpkgs/pull/176835).
   you will also have to set `tokenserver.fxa_oauth_server_url` as
   noted in `Rocket.toml`, otherwise actual sync will not work.

minor skulk should be run under a dedicated user account that is *not* shared
with syncstorage-rs. it should definitely *not* be run as root. as previously
mentioned, deploying this on the internet **must** be done through a TLS proxy
since minor skulk itself does not handle TLS in any way.

# configuring firefox desktop

go to `about:config` and set `identity.fxaccounts.autoconfig.uri` to
the address of your server, e.g. `http://localhost:8000` if you're just
starting minor-skulk locally for testing. if you are not using https you
must also create a `identity.fxaccounts.allowHttp` setting with value `true`,
otherwise firefox will not accept the config. restart firefox for the
changes to take effect, then create an account or log in as usual.

# configuring firefox android

go to the settings menu, from there to the About page, tap the logo a lot until
the debug menu is enabled. go back to the settings menu, scroll all the way to
the top, enter your minor-skulk url as the custom account server. once
configured thusly firefox will exit. restart it, select "sign in to sync", and
use the email option. (QR code pairing is not yet implemented)

**NOTE**: the account server *must* run over https and with a certificate
firefox trusts, or logins will not function.

# license

this project is licensed under the terms of the GNU General Public License
version 3.0 only. portions of this project (the fxa javascript client,
`web/js/browser`) is a fork of mozilla's
[fxa-auth-client](https://github.com/mozilla/fxa/blob/main/packages/fxa-auth-client)
and is licensed under the terms of the Mozilla Public License, v. 2.0.
