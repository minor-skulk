{ stdenv
, rustPlatform
, rust
, lib
, openssl
, pkg-config
, postgresql
, postgresqlTestHook
, python3
, zlib
}:

rustPlatform.buildRustPackage rec {
  pname = "minor-skulk";
  version = "0.1.1";

  src = lib.cleanSource ./.;
  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [
    pkg-config
  ];
  buildInputs = [
    openssl
    zlib
  ];

  postPatch = ''
    patchShebangs ./tests/run.sh
  '';

  nativeCheckInputs = [
    openssl
    postgresql
    postgresqlTestHook
    (python3.withPackages (p: [ p.pytest p.pyfxa p.requests p.http-ece p.aiosmtpd ]))
  ];

  preCheck = ''
    openssl ecparam -genkey -name prime256v1 -out private_key.pem
  '';

  # test config for postgres hook and integration tests
  PGDATABASE = "testdb";
  PGUSER = "testuser";
  ROCKET_DATABASE_URL = "postgres:///${PGDATABASE}?user=${PGUSER}";
  ROCKET_LOCATION = "http://localhost:8000";
  ROCKET_TOKEN_SERVER_LOCATION = "http://localhost:5000";
  ROCKET_VAPID_KEY = "private_key.pem";
  ROCKET_VAPID_SUBJECT = "undefined"; # not needed for tests
  ROCKET_MAIL_FROM = "minor skulk <noreply@localhost>";
  ROCKET_LOG_LEVEL = "debug";
}
