self:

{ config, lib, pkgs, ... }:

let
  format = pkgs.formats.toml {};
  cfg = config.services.minorskulk;

  config_file = format.generate "Rocket.toml" {
    default = {
      ident = "minor skulk";
      limits.string = "32KiB";
      limits.bytes = "128KiB";
    }
    // cfg.settings
    // {
      vapid_key = "vapid.key";
    };
  };

  pkg = self.packages.${config.nixpkgs.system}.default;
in
{
  options.services.minorskulk = with lib; {
    enable = mkEnableOption (mdDoc "the minor-skulk firefox accounts server");

    settings = mkOption {
      type = types.submodule {
        freeformType = format.type;

        options = {
          database_url = mkOption {
            type = types.str;
            default = "postgres:///minorskulk";
            description = mdDoc ''
              Database to use for storage. Only postgres is supported at this time.
            '';
          };

          location = mkOption {
            type = types.str;
            example = "https://minorskulk.my.domain";
            description = mdDoc ''
              Web location of the API endpoints.
            '';
          };

          token_server_location = mkOption {
            type = types.str;
            example = "https://syncstorage.my.domain";
            description = mdDoc ''
              Web location of the syncstorage token server.
            '';
          };

          vapid_subject = mkOption {
            type = types.str;
            example = "minorskulk@my.domain";
            description = mdDoc ''
              VAPID subject added to push messages sent over mozilla push services.
            '';
          };

          vapid_key = mkOption {
            type = types.path;
            example = "/etc/secrets/minorskulk-vapid-key";
            description = mdDoc ''
              VAPID key used to sign push messages sent over mozilla push services.
            '';
          };

          mail_from = mkOption {
            type = types.str;
            example = "minorskulk@my.domain";
            description = mdDoc ''
              Sender address for generated emails.
            '';
          };
        };
      };
      description = mdDoc ''
        Settings for minor-skulk. Will we written to Rocket.toml.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    services.postgresql = {
      enable = true;
      ensureDatabases = [ "minorskulk" ];
      ensureUsers = [ {
        name = "minorskulk";
        ensureDBOwnership = true;
      } ];
    };

    systemd.services.minorskulk = {
      wantedBy = [ "multi-user.target" ];
      requires = [ "postgresql.service" ];
      after = [ "postgresql.service" ];

      path = [ pkg ];
      script = ''
        umask 0077
        cd "$RUNTIME_DIRECTORY"
        cp ${config_file} Rocket.toml
        cp "$CREDENTIALS_DIRECTORY/vapid" vapid.key
        exec minorskulk
      '';

      serviceConfig = {
        User = "minorskulk";
        Group = "minorskulk";
        LoadCredential = "vapid:${cfg.settings.vapid_key}";

        RuntimeDirectory = "minorskulk";
        AmbientCapabilities = [ "" ];
        CapabilityBoundingSet = [ "" ];
        DynamicUser = true;
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        NoNewPrivileges = true;
        PrivateDevices = true;
        PrivateTmp = true;
        ProcSubset = "pid";
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectProc = "invisible";
        ProtectSystem = true;
        RemoveIPC = true;
        RestrictAddressFamilies = "AF_INET AF_INET6 AF_UNIX";
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = [
          "@system-service"
          "~ @resources @privileged"
        ];
        UMask = "0077";
      };
    };
  };
}
