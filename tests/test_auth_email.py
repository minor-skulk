import pytest
from fxa.errors import ClientError

from api import *

class TestUnverified:
    def test_status_unverified(self, unverified_account):
        resp = unverified_account.get_a("/recovery_email/status")
        assert resp == {
            'email': unverified_account.email,
            'emailVerified': False,
            'sessionVerified': False,
            'verified': False
        }

    @pytest.mark.parametrize("args,code,errno,error,message", [
        ({ 'uid': '00', 'code': "" },
        400, 107, 'Bad Request', 'invalid parameter in request body'),
        ({ 'id': '00' * 16, 'code': 0 },
        400, 107, 'Bad Request', 'invalid parameter in request body'),
        ({ 'id': '00' * 16, 'code': "", 'extra': 0 },
        400, 107, 'Bad Request', 'invalid parameter in request body'),
    ])
    def test_verify_code_invalid(self, unverified_account, args, code, errno, error, message):
        with pytest.raises(ClientError) as e:
            unverified_account.post_a("/recovery_email/verify_code", args)
        assert e.value.details == {'code': code, 'errno': errno, 'error': error, 'message': message}

class TestVerified:
    def test_status_noauth(self, client, refresh_token):
        with pytest.raises(ClientError) as e:
            client.post_a("/recovery_email/status")
        assert e.value.details == {
            'code': 401,
            'errno': 109,
            'error': 'Unauthorized',
            'message': 'invalid request signature'
        }
        with pytest.raises(ClientError) as e:
            refresh_token.post_a("/recovery_email/status")
        assert e.value.details == {
            'code': 401,
            'errno': 109,
            'error': 'Unauthorized',
            'message': 'invalid request signature'
        }

    def test_status_verified(self, account):
        resp = account.get_a("/recovery_email/status")
        assert resp == {
            'email': account.email,
            'emailVerified': True,
            'sessionVerified': True,
            'verified': True
        }

    def test_verify_code(self, account):
        # fixture does all the work
        pass

def test_verify_code_reuse(client, mail_server):
    s = client.create_account("test@test", "")
    (to, body) = mail_server.wait()
    assert to == ["test@test"]
    data = json.loads(base64.urlsafe_b64decode(body.split("#/verify/", maxsplit=1)[1]).decode('utf8'))
    s.post_a('/recovery_email/verify_code', { 'uid': data['uid'], 'code': data['code'] })
    with pytest.raises(ClientError) as e:
        s.post_a('/recovery_email/verify_code', { 'uid': data['uid'], 'code': data['code'] })
    s.destroy_account("test@test", "")
    assert e.value.details == {
        'code': 400,
        'errno': 105,
        'error': 'Bad Request',
        'message': 'invalid verification code'
    }

def test_resend_code(client, mail_server):
    s = client.create_account("test@test", "")
    (to, body) = mail_server.wait()
    assert to == ["test@test"]
    data = json.loads(base64.urlsafe_b64decode(body.split("#/verify/", maxsplit=1)[1]).decode('utf8'))
    s.post_a('/recovery_email/resend_code', {})
    (to2, body2) = mail_server.wait()
    assert to == to2
    assert body == body2
    s.post_a('/recovery_email/verify_code', { 'uid': data['uid'], 'code': data['code'] })
    with pytest.raises(ClientError) as e:
        s.post_a('/recovery_email/resend_code', {})
        (to, body) = mail_server.wait()
        assert to == ["test@test"]
        data = json.loads(base64.urlsafe_b64decode(body.split("#/verify/", maxsplit=1)[1]).decode('utf8'))
    s.destroy_account("test@test", "")
    assert e.value.details == {
        'code': 400,
        'errno': 105,
        'error': 'Bad Request',
        'message': 'invalid verification code'
    }
