import pytest
from fxa.crypto import derive_key, quick_stretch_password
from fxa.errors import ClientError

from api import *

class TestPasswordInvalid:
    @pytest.mark.parametrize("args", [
        { 'email': "", 'oldAuthPW': '00' * 32 },
        { 'email': "test0@test", 'oldAuthPW': '00' },
        { 'email': "test0@test", 'oldAuthPW': '00' * 32, 'extra': 0 },
    ])
    def test_change_start_invalid(self, account, args):
        with pytest.raises(ClientError) as e:
            account.post_a("/password/change/start", args)
        assert e.value.details == {
            'code': 400,
            'errno': 107,
            'error': 'Bad Request',
            'message': 'invalid parameter in request body'
        }

    def test_change_start_badaccount(self, account):
        with pytest.raises(ClientError) as e:
            account.post_a("/password/change/start", { 'email': "test0@test", 'oldAuthPW': '00' * 32 })
        assert e.value.details == {
            'code': 400,
            'errno': 102,
            'error': 'Bad Request',
            'message': 'unknown account'
        }
        with pytest.raises(ClientError) as e:
            account.post_a("/password/change/start", { 'email': account.email.upper(), 'oldAuthPW': '00' * 32 })
        assert e.value.details == {
            'code': 400,
            'errno': 120,
            'error': 'Bad Request',
            'message': 'incorrect email case'
        }

    def test_change_start_badpw(self, account):
        with pytest.raises(ClientError) as e:
            account.post_a("/password/change/start", { 'email': account.email, 'oldAuthPW': '00' * 32 })
        assert e.value.details == {
            'code': 400,
            'errno': 103,
            'error': 'Bad Request',
            'message': 'incorrect password'
        }

    @pytest.mark.parametrize("args", [
        { 'email': "" },
        { 'email': "test0@test", 'extra': 0 },
    ])
    def test_forgot_start_invalid(self, account, args):
        with pytest.raises(ClientError) as e:
            account.post_a("/password/forgot/send_code", args)
        assert e.value.details == {
            'code': 400,
            'errno': 107,
            'error': 'Bad Request',
            'message': 'invalid parameter in request body'
        }

    def test_change_forgot_badaccount(self, account):
        with pytest.raises(ClientError) as e:
            account.post_a("/password/forgot/send_code", { 'email': "test0@test" })
        assert e.value.details == {
            'code': 400,
            'errno': 102,
            'error': 'Bad Request',
            'message': 'unknown account'
        }
        with pytest.raises(ClientError) as e:
            account.post_a("/password/forgot/send_code", { 'email': account.email.upper() })
        assert e.value.details == {
            'code': 400,
            'errno': 120,
            'error': 'Bad Request',
            'message': 'incorrect email case'
        }

def test_change_start_unverified(unverified_account):
    with pytest.raises(ClientError) as e:
        unverified_account.post_a("/password/change/start", {
            'email': unverified_account.email,
            'oldAuthPW': '00' * 32
        })
    assert e.value.details == {
        'code': 400,
        'errno': 104,
        'error': 'Bad Request',
        'message': 'unverified account'
    }

@pytest.fixture
def change_token(account):
    pw = auth_pw(account.email, "")
    resp = account.post_a("/password/change/start", { 'email': account.email, 'oldAuthPW': pw })
    assert 'keyFetchToken' in resp
    return PasswordChange(account.client, resp['passwordChangeToken'])

class TestChangeInvalid:
    @pytest.mark.parametrize("args", [
        { 'authPW': '00', 'wrapKb': '00' * 32, 'sessionToken': '00' * 32, },
        { 'authPW': '00' * 32, 'wrapKb': '00', 'sessionToken': '00' * 32, },
        { 'authPW': '00' * 32, 'wrapKb': '00' * 32, 'sessionToken': '00', },
    ])
    def test_change_finish_invalid(self, change_token, args):
        with pytest.raises(ClientError) as e:
            change_token.post_a("/password/change/finish", args)
        assert e.value.details == {
            'code': 400,
            'errno': 107,
            'error': 'Bad Request',
            'message': 'invalid parameter in request body'
        }

def test_change_finish(account, change_token, mail_server):
    pw = auth_pw(account.email, "new")
    change_token.post_a("/password/change/finish", {
        'authPW': pw,
        'wrapKb': '00' * 32,
    })
    account.password = "new" # for fixture teardown
    (to, body) = mail_server.wait()
    assert account.email in to
    assert 'password has been changed' in body

    # just do a login test to see that the password was really changed
    account.login(account.email, "new")
    with pytest.raises(ClientError) as e:
        account.login(account.email, "")
    assert e.value.details == {
        'code': 400,
        'errno': 103,
        'error': 'Bad Request',
        'message': 'incorrect password'
    }

def test_change_finish_twice(account, change_token, mail_server):
    pw = auth_pw(account.email, "new")
    change_token.post_a("/password/change/finish", {
        'authPW': pw,
        'wrapKb': '00' * 32,
    })
    account.password = "new" # for fixture teardown

    with pytest.raises(ClientError) as e:
        change_token.post_a("/password/change/finish", {
                'authPW': pw,
                'wrapKb': '00' * 32,
        })
    assert e.value.details == {
        'code': 401,
        'errno': 109,
        'error': 'Unauthorized',
        'message': 'invalid request signature'
    }

def test_change_forgot_unverified(unverified_account):
    with pytest.raises(ClientError) as e:
        unverified_account.post_a("/password/forgot/send_code", { 'email': unverified_account.email })
    assert e.value.details == {
        'code': 400,
        'errno': 104,
        'error': 'Bad Request',
        'message': 'unverified account'
    }

@pytest.mark.parametrize("args", [
    { 'code': '', 'extra': 0, },
])
def test_forgot_finish_invalid(change_token, args):
    with pytest.raises(ClientError) as e:
        change_token.post_a("/password/forgot/send_code", args)
    assert e.value.details == {
        'code': 400,
        'errno': 107,
        'error': 'Bad Request',
        'message': 'invalid parameter in request body'
    }

def test_forgot_finish_badcode(account, forgot_token, mail_server):
    with pytest.raises(ClientError) as e:
        resp = forgot_token.post_a("/password/forgot/verify_code", { 'code': '' })
    assert e.value.details == {
        'code': 400,
        'errno': 105,
        'error': 'Bad Request',
        'message': 'invalid verification code'
    }

def test_forgot_finish(account, forgot_token, mail_server):
    resp = forgot_token.post_a("/password/forgot/verify_code", { 'code': forgot_token.code })
    assert 'accountResetToken' in resp

def test_forgot_finish_twice(account, forgot_token, mail_server):
    forgot_token.post_a("/password/forgot/verify_code", { 'code': forgot_token.code })

    with pytest.raises(ClientError) as e:
        forgot_token.post_a("/password/forgot/verify_code", { 'code': forgot_token.code })
    assert e.value.details == {
        'code': 401,
        'errno': 109,
        'error': 'Unauthorized',
        'message': 'invalid request signature'
    }
