import pytest
from fxa.errors import ClientError

from api import *

def test_session_loggedout(client):
    with pytest.raises(ClientError) as e:
        client.post("/session/destroy")
    assert e.value.details == {
        'code': 401,
        'errno': 109,
        'error': 'Unauthorized',
        'message': 'invalid request signature'
    }

class TestSession:
    def test_status(self, account):
        resp = account.get_a("/session/status")
        assert resp == { 'state': '', 'uid': account.props['uid'] }

    def test_resend(self, account, mail_server):
        c = account.login(account.email, "")
        (to, body) = mail_server.wait()
        assert to == [account.email]
        c.post_a("/session/resend_code", {})
        (to2, body2) = mail_server.wait()
        assert to == to2
        assert body == body2

    @pytest.mark.parametrize("args", [
        { 'custom_session_id': '00' },
        { 'extra': '00' },
    ])
    def test_session_invalid(self, account, args):
        with pytest.raises(ClientError) as e:
            account.post_a("/session/destroy", args)
        assert e.value.details == {
            'code': 400,
            'errno': 107,
            'error': 'Bad Request',
            'message': 'invalid parameter in request body'
        }

    def test_session_noid(self, account):
        with pytest.raises(ClientError) as e:
            account.post_a("/session/destroy", { 'custom_session_id': '0' * 64 })
        assert e.value.details == {
            'code': 400,
            'errno': 123,
            'error': 'Bad Request',
            'message': 'unknown device'
        }

    def test_session_destroy_other(self, account, account2):
        with pytest.raises(ClientError) as e:
            account.post_a("/session/destroy", { 'custom_session_id': account2.auth.id })
        assert e.value.details == {
            'code': 400,
            'errno': 123,
            'error': 'Bad Request',
            'message': 'unknown device'
        }

def test_session_destroy_unverified(unverified_account):
    unverified_account.destroy_session()
    unverified_account.destroy_session = lambda *args: None

def test_session_destroy(account):
    s = account.login(account.email, "")
    s.destroy_session()
