import pytest
from fxa.errors import ClientError

from api import *

@pytest.fixture
def profile(account):
    return account.profile()

class TestProfile:
    def test_profile_noauth(self, profile):
        with pytest.raises(ClientError) as e:
            profile.get("/profile")
        assert e.value.details == {
            'code': 403,
            'errno': 100,
            'error': 'Forbidden',
            'message': 'unauthorized'
        }

    def test_display_name_noauth(self, profile):
        with pytest.raises(ClientError) as e:
            profile.post("/display_name", {'displayName': 'foo'})
        assert e.value.details == {
            'code': 403,
            'errno': 100,
            'error': 'Forbidden',
            'message': 'unauthorized'
        }

    def test_avatar_upload_noauth(self, profile):
        with pytest.raises(ClientError) as e:
            profile.post("/avatar/upload", "foo", headers={'content-type': 'image/png'})
        assert e.value.details == {
            'code': 403,
            'errno': 100,
            'error': 'Forbidden',
            'message': 'unauthorized'
        }

    def test_avatar_delete_noauth(self, profile):
        with pytest.raises(ClientError) as e:
            profile.delete("/avatar/00000000000000000000000000000000")
        assert e.value.details == {
            'code': 403,
            'errno': 100,
            'error': 'Forbidden',
            'message': 'unauthorized'
        }

    def test_profile(self, account, profile):
        resp = profile.get_a("/profile")
        assert resp == {
            'amrValues': None,
            'avatar': f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000',
            'avatarDefault': True,
            'displayName': None,
            'email': account.email,
            'locale': None,
            'subscriptions': None,
            'twoFactorAuthentication': False,
            'uid': account.props['uid']
        }

    def test_display_name(self, account, profile):
        resp = profile.get_a("/profile")
        assert resp == {
            'amrValues': None,
            'avatar': f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000',
            'avatarDefault': True,
            'displayName': None,
            'email': account.email,
            'locale': None,
            'subscriptions': None,
            'twoFactorAuthentication': False,
            'uid': account.props['uid']
        }
        profile.post_a("/display_name", {'displayName': 'foo'})
        resp = profile.get_a("/profile")
        assert resp == {
            'amrValues': None,
            'avatar': f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000',
            'avatarDefault': True,
            'displayName': 'foo',
            'email': account.email,
            'locale': None,
            'subscriptions': None,
            'twoFactorAuthentication': False,
            'uid': account.props['uid']
        }

    def test_avatar(self, account, profile):
        resp = profile.get_a("/avatar")
        assert resp == {
            'avatar': f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000',
            'avatarDefault': True,
            'id': '00000000000000000000000000000000'
        }

    def test_avatar_upload(self, account, profile):
        # server does not parse the bytes
        profile.post_a("/avatar/upload", "foo", headers={'content-type': 'image/png'})
        resp = profile.get_a("/avatar")
        new_id = resp['id']
        assert resp['avatar'] != f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000'
        assert not resp['avatarDefault']
        assert resp['id'] != '00000000000000000000000000000000'
        resp = profile.get_a("/profile")
        assert resp['avatar'] != f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000'
        assert not resp['avatarDefault']

    def test_avatar_delete(self, account, profile):
        # server does not parse the bytes
        profile.post_a("/avatar/upload", "foo", headers={'content-type': 'image/png'})
        resp = profile.get_a("/avatar")
        new_id = resp['id']
        profile.delete_a(f"/avatar/{new_id}")
        resp = profile.get_a("/avatar")
        assert resp == {
            'avatar': f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000',
            'avatarDefault': True,
            'id': '00000000000000000000000000000000'
        }
        resp = profile.get_a("/profile")
        assert resp == {
            'amrValues': None,
            'avatar': f'http://localhost:{API_PORT}/avatars/00000000000000000000000000000000',
            'avatarDefault': True,
            'displayName': resp['displayName'], # ignore this field
            'email': account.email,
            'locale': None,
            'subscriptions': None,
            'twoFactorAuthentication': False,
            'uid': account.props['uid']
        }
