import asyncio
import quopri
from aiosmtpd.controller import Controller

class PrintHandler:
    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        envelope.rcpt_tos.append(address)
        return '250 OK'

    async def handle_DATA(self, server, session, envelope):
        print('Message from %s' % envelope.mail_from)
        print('Message for %s' % envelope.rcpt_tos)
        print('Message data:\n')
        headers, body = envelope.content.decode('utf8').split("\r\n\r\n", maxsplit=1)
        if "Content-Transfer-Encoding: quoted-printable" in headers:
                body = quopri.decodestring(body).decode('utf8')
        print(headers)
        print()
        print(body)
        print()
        print('End of message')
        return '250 Message accepted for delivery'

if __name__ == '__main__':
        controller = Controller(PrintHandler(), hostname="localhost", port=2525)
        controller.start()
        input()
