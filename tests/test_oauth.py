import pytest
from fxa.errors import ClientError

from api import *

@pytest.fixture
def oauth():
    return Oauth()

@pytest.fixture
def access_token(account):
    body = {
        "client_id": "5882386c6d801776",
        "ttl": 60,
        "grant_type": "fxa-credentials",
        "access_type": "online",
        "scope": "profile",
    }
    resp = account.post_a("/oauth/token", body)
    return resp['access_token']

@pytest.mark.parametrize("args,code,errno,error,message", [
    ({"access_token": "0"},
     400, 109, 'Bad Request', 'invalid request parameter'),
    ({"refresh_token": "0"},
     400, 109, 'Bad Request', 'invalid request parameter'),
    ({"token": "0"},
     400, 109, 'Bad Request', 'invalid request parameter'),
])
def test_destroy_invalid(oauth, args, code, errno, error, message):
    with pytest.raises(ClientError) as e:
        oauth.post("/destroy", args)
    assert e.value.details == {'code': code, 'errno': errno, 'error': error, 'message': message}

class TestOauth:
    def test_destroy_access(self, oauth, access_token):
        oauth.post("/verify", {'token': access_token})
        oauth.post("/destroy", {'access_token': access_token})
        with pytest.raises(ClientError) as e:
            oauth.post("/verify", {'token': access_token})
        assert e.value.details == {
            'code': 400,
            'errno': 109,
            'error': 'Bad Request',
            'message': 'invalid request parameter'
        }

    def test_destroy_refresh(self, oauth, refresh_token):
        refresh_token.get_a("/account/devices")
        oauth.post("/destroy", {'refresh_token': refresh_token.bearer})
        with pytest.raises(ClientError) as e:
            refresh_token.get_a("/account/devices")
        assert e.value.details == {
            'code': 401,
            'errno': 109,
            'error': 'Unauthorized',
            'message': 'invalid request signature'
        }

    def test_destroy_any(self, oauth, access_token, refresh_token):
        oauth.post("/verify", {'token': access_token})
        oauth.post("/destroy", {'token': access_token})
        with pytest.raises(ClientError) as e:
            oauth.post("/verify", {'token': access_token})
        assert e.value.details == {
            'code': 400,
            'errno': 109,
            'error': 'Bad Request',
            'message': 'invalid request parameter'
        }

        refresh_token.get_a("/account/devices")
        oauth.post("/destroy", {'token': refresh_token.bearer})
        with pytest.raises(ClientError) as e:
            refresh_token.get_a("/account/devices")
        assert e.value.details == {
            'code': 401,
            'errno': 109,
            'error': 'Unauthorized',
            'message': 'invalid request signature'
        }

    def test_oauth_verify(self, account, oauth, access_token):
        assert oauth.post("/verify", {'token': access_token}) == {
            'user': account.props['uid'],
            'client_id': "5882386c6d801776",
            'scope': ['profile'],
        }

    def test_oauth_verify_refresh(self, oauth, refresh_token):
        with pytest.raises(ClientError) as e:
            oauth.post("/verify", {'token': refresh_token.bearer})
        assert e.value.details == {
            'code': 400,
            'errno': 109,
            'error': 'Bad Request',
            'message': 'invalid request parameter'
        }
