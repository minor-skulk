/// <reference types="./lib/types" />
export declare function deriveHawkCredentials(token: hexstring, context: string): Promise<{
    id: string;
    key: Uint8Array;
    bundleKey: string;
}>;
export declare function hawkHeader(method: string, uri: string, options: {
    credentials: {
        id: string;
        key: Uint8Array;
    };
    payload?: string;
    timestamp?: number;
    nonce?: string;
    contentType?: string;
    localtimeOffsetMsec?: number;
}): Promise<string>;
export declare function header(method: string, uri: string, token: string, kind: string, options: {
    payload?: string;
    timestamp?: number;
    nonce?: string;
    contentType?: string;
    localtimeOffsetMsec?: number;
}): Promise<Headers>;
