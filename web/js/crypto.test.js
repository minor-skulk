async function testCrypto() {
    let keys = await deriveScopedKey(
        "8b2e1303e21eee06a945683b8d495b9bf079ca30baa37eb8392d9ffa4767be45",
        "aeaa1725c7a24ff983c6295725d5fc9b",
        "app_key:https%3A//example.com",
        "517d478cb4f994aa69930416648a416fdaa1762c5abf401a2acf11a0f185e98d",
        1510726317);
    if (keys.k != "Kkbk1_Q0oCcTmggeDH6880bQrxin2RLu5D00NcJazdQ") throw "assert";
    if (keys.kid != "1510726317-Voc-Eb9IpoTINuo9ll7bjA") throw "assert";
    if (keys.kty != "oct") throw "assert";

    let keys_jwk = "eyJjcnYiOiJQLTI1NiIsImt0eSI6IkVDIiwieCI6IlNpQm42dWViamlnbVF" +
        "xdzRUcE56czNBVXlDYWUxX3NHMmI5RnpocTNGeW8iLCJ5IjoicTk5WHExUld" +
        "OVEZwazk5cGRRT1NqVXZ3RUxzczUxUGttQUdDWGhMZk1WNCJ9";
    let bundle = {
        app_key: {
            k: "Kkbk1_Q0oCcTmggeDH6880bQrxin2RLu5D00NcJazdQ",
            kid: "1510726317-Voc-Eb9IpoTINuo9ll7bjA",
            kty: "oct"
        }
    };
    let local_key = {
        "crv": "P-256",
        "kty": "EC",
        "d": "X9tJG0Ue55tuepC-6msMg04Qv5gJtL95AIJ0X0gDj8Q",
        "x": "N4zPRazB87vpeBgHzFvkvd_48owFYYxEVXRMrOU6LDo",
        "y": "4ncUxN6x_xT1T1kzy_S_V2fYZ7uUJT_HVRNZBLJRsxU"
    };
    let iv = new Uint8Array([0xff, 0x4b, 0x18, 0x7f, 0xb1, 0xdd, 0x5a, 0xe4, 0x6f, 0xd9, 0xc3, 0x34]);
    let k = await crypto.subtle.importKey(
        "jwk",
        local_key,
        { name: "ECDH", namedCurve: "P-256" },
        true,
        ['deriveKey']);
    console.log(await encryptScopedKeys(
        bundle,
        keys_jwk,
        { publicKey: k, privateKey: k },
        iv));

    console.log(await deriveScopedKey(
        "eaf9570b7219a4187d3d6bf3cec2770c2e0719b7cc0dfbb38243d6f1881675e9",
        "aeaa1725c7a24ff983c6295725d5fc9b",
        "https://identity.mozilla.com/apps/oldsync",
        "0000000000000000000000000000000000000000000000000000000000000000",
        1510726317123
    ));
}
