alter procedure prune_expired_tokens() rename to prune_expired_tokens_1;

create procedure prune_expired_tokens()
language sql
begin atomic
       delete from key_fetch where expires_at <= now();
       -- give oauth tokens a grace period, otherwise firefox will log an error
       -- once per hour trying to destroy a token that has already been timed out.
       delete from oauth_token where expires_at + '1 day'::interval <= now();
       delete from oauth_authorization where expires_at <= now();
       delete from device_commands where expires <= now();
       delete from invite_codes where expires_at <= now();
end;
