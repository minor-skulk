use std::borrow::Cow;

use rocket::{
    http::{Header, Status},
    request::{self, FromRequest},
    response::{self, Responder},
    Request,
};

pub(crate) struct Etagged<'r, T>(pub T, pub Cow<'r, str>);

impl<'r, 'o: 'r, T: Responder<'r, 'o>> Responder<'r, 'o> for Etagged<'o, T> {
    fn respond_to(self, r: &'r Request<'_>) -> response::Result<'o> {
        let mut resp = self.0.respond_to(r)?;
        resp.set_header(Header::new("etag", self.1));
        Ok(resp)
    }
}

pub(crate) struct Immutable<T>(pub T);

impl<'r, 'o: 'r, T: Responder<'r, 'o>> Responder<'r, 'o> for Immutable<T> {
    fn respond_to(self, r: &'r Request<'_>) -> response::Result<'o> {
        let mut resp = self.0.respond_to(r)?;
        resp.set_header(Header::new("cache-control", "public, max-age=604800, immutable"));
        Ok(resp)
    }
}

pub(crate) struct IfNoneMatch<'r>(pub &'r str);

#[async_trait]
impl<'r> FromRequest<'r> for IfNoneMatch<'r> {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match req.headers().get_one("if-none-match") {
            Some(h) => request::Outcome::Success(Self(h)),
            None => request::Outcome::Forward(Status::Ok),
        }
    }
}
