use rocket::serde::json::Json;
use serde::{Deserialize, Serialize};

pub(crate) mod auth;
pub(crate) mod oauth;
pub(crate) mod profile;

#[derive(Clone, Copy, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Empty {}

pub const EMPTY: Json<Empty> = Json(Empty {});
