use std::{collections::HashMap, path::PathBuf};

use rocket::http::{ContentType, Status};
use sha2::{Digest, Sha256};

use crate::cache::{Etagged, IfNoneMatch};

struct Entry {
    data: &'static str,
    hash: String,
}

fn enter(data: &'static str) -> Entry {
    let mut sha = Sha256::new();
    sha.update(data.as_bytes());
    let hash = base64::encode(sha.finalize());
    Entry { data, hash }
}

lazy_static! {
    static ref JS: HashMap<&'static str, Entry> = {
        let mut m = HashMap::new();
        m.insert("main", enter(include_str!("../web/js//main.js")));
        m.insert("crypto", enter(include_str!("../web/js//crypto.js")));
        m.insert("auth-client/browser", enter(include_str!("../web/js//browser/browser.js")));
        m.insert("auth-client/lib/client", enter(include_str!("../web/js//browser/lib/client.js")));
        m.insert("auth-client/lib/crypto", enter(include_str!("../web/js//browser/lib/crypto.js")));
        m.insert("auth-client/lib/hawk", enter(include_str!("../web/js//browser/lib/hawk.js")));
        m.insert(
            "auth-client/lib/recoveryKey",
            enter(include_str!("../web/js//browser/lib/recoveryKey.js")),
        );
        m.insert("auth-client/lib/utils", enter(include_str!("../web/js//browser/lib/utils.js")));
        m
    };
}

#[get("/<name..>")]
pub(crate) async fn static_js(
    name: PathBuf,
    inm: Option<IfNoneMatch<'_>>,
) -> (Status, Result<(ContentType, Etagged<'_, &'static str>), ()>) {
    let entry = JS.get(name.to_string_lossy().as_ref());
    match entry {
        Some(e) => match inm {
            Some(h) if h.0 == e.hash => (Status::NotModified, Err(())),
            _ => {
                (Status::Ok, Ok((ContentType::JavaScript, Etagged(e.data, e.hash.as_str().into()))))
            },
        },
        _ => (Status::NotFound, Err(())),
    }
}
