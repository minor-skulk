use minor_skulk::build;

#[rocket::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();

    let _ = build(rocket::build()).await?.launch().await?;
    Ok(())
}
