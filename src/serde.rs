pub mod as_hex {
    use serde::{de, Deserialize, Deserializer, Serializer};

    pub fn serialize<const N: usize, S: Serializer>(
        b: &[u8; N],
        ser: S,
    ) -> Result<S::Ok, S::Error> {
        ser.serialize_str(&hex::encode(b))
    }

    pub fn deserialize<'de, const N: usize, D: Deserializer<'de>>(
        des: D,
    ) -> Result<[u8; N], D::Error> {
        let raw = <String as Deserialize>::deserialize(des)?;
        let mut result = [0; N];
        hex::decode_to_slice(&raw, &mut result).map_err(|_| {
            de::Error::invalid_value(de::Unexpected::Other("non-hex string"), &"a hex string")
        })?;
        Ok(result)
    }
}
