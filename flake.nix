{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
  };

  outputs = { self, nixpkgs }:
    let
      systems = { "x86_64-linux" = {}; };
      combine = fn: with builtins;
        let
          parts = mapAttrs (s: _: fn (nixpkgs.legacyPackages.${s})) systems;
          keys = foldl' (a: b: a // b) {} (attrValues parts);
        in
          mapAttrs (k: _: mapAttrs (s: _: parts.${s}.${k} or {}) systems) keys;
    in
      {
        nixosModule = import ./module.nix self;
      } // combine (pkgs: rec {
        packages = rec {
          minor-skulk = pkgs.callPackage ./default.nix {};
          default = minor-skulk;
        };

        devShells.default = pkgs.mkShell {
          inputsFrom = [ packages.default ];
          packages = with pkgs; [
            # cargo-audit
            rustfmt
            rust-analyzer
            clippy
            sqlx-cli
          ];
        };
      });
}
